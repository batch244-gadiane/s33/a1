//Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos")
	.then((response)=> response.json())
	.then(json => {console.log(json.map(todo => (todo.title)))})
	


//Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

//5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((response) => console.log(`The item"${response}" on the list has a status of ${response.completed}`));

//6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.


//7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos", 
	{
		method: "POST",
		
		headers: {
			"Content-Type": "application/json"
		},

		body: JSON.stringify({
			title: "Created a to do list item",
			completed: false,
			userId: 1
		})
	}
)
.then(response => response.json())
.then(response => console.log(response));

//8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "PUT",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			id: 1,
			title: "Updated To Do List",
			description: "To update the my to do list with a different data strcuture.",
			status: "Pending",
			dataCompleted: "Pending",
			userId: 1
		})
	}
)
.then(response => response.json())
.then(response => console.log(response));

//10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
//11. Update a to do list item by changing the status to complete and add a date when the status was changed.

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
	  	status: 'Complete',
	  	dateCompleted: '01/12/2023'
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [Section] Deleting a to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'DELETE',
});